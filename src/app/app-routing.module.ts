import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {
    path: 'menu',
    loadChildren: () => import('./menu/menu.module').then( m => m.MenuPageModule)
  },
  
  {
    path: '',
    redirectTo: 'menu',
    pathMatch: 'full'
  },
  {
    path: 'menu/:user',
    loadChildren: () => import('./menu/menu.module').then( m => m.MenuPageModule)
  },

  {
    path: 'lista-clientes',
    loadChildren: () => import('./shared/pages/lista-clientes/lista-clientes.module').then( m => m.ListaClientesPageModule)
  },
  {
    path: 'ver-cliente/:id',
    loadChildren: () => import('./shared/pages/ver-cliente/ver-cliente.module').then( m => m.VerClientePageModule)
  },
  {
    path: 'lista-vehiculos',
    loadChildren: () => import('./shared/pages/lista-vehiculos/lista-vehiculos.module').then( m => m.ListaVehiculosPageModule)
  },
  {
    path: 'ver-vehiculo/:id',
    loadChildren: () => import('./shared/pages/ver-vehiculo/ver-vehiculo.module').then( m => m.VerVehiculoPageModule)
  },
  {
    path: 'insert-vehiculo',
    loadChildren: () => import('./shared/pages/insert-vehiculo/insert-vehiculo.module').then( m => m.InsertVehiculoPageModule)
  },









  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
