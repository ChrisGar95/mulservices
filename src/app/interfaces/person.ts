export interface Person {
    cedula : String;
    nombre : String;
    direccion : String;
    fechaNacimiento : String;
    sexo: String;
    experiencia: String;
    proyeccion: String;
    titulo3Nivel: String;
    titulo4Nivel: String;
}