import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-insert-vehiculo',
  templateUrl: './insert-vehiculo.page.html',
  styleUrls: ['./insert-vehiculo.page.scss'],
})
export class InsertVehiculoPage implements OnInit {

  id:any;
  idMarca:any;
  idCategoria:any;
  listadoMarcas:any;
  listadoCategorias:any;
  modelo:any;
  anio:any;
  version:any;
  color:any;
  imagen:any;
  precio:any;
  
  categorias;
  marcas;

  constructor
  (
    public router: Router,
    private route: ActivatedRoute,
    private http:HttpClient,
    public alertController: AlertController,

    
  ) 
  {
    this.cargarInformacion();
    
  }

  async ngOnInit() {
    
  }

  async cargarInformacion() 
  {
    
    this.http.get("http://localhost:8081/api/getMarca").subscribe(snap => {
      this.listadoMarcas = snap;
    });

    

    this.http.get("http://localhost:8081/api/getCategoria").subscribe(snap2 => {
      this.listadoCategorias = snap2;
    });
  }

  creaDatosVehiculo()
  {
    this.idCategoria=this.idCategoria.substring(0,1);
    this.idMarca=this.idMarca.substring(0,1);
    console.log(this.idCategoria);
    console.log(this.idMarca);
    
    var fd = new FormData();

    this.id=0;
    fd.append('id',this.id);
    fd.append('modelo', this.modelo);
    fd.append('anio', this.anio);
    fd.append('version', this.version);
    fd.append('color', this.color);
    fd.append('precio', this.precio);
    fd.append('imagen', this.imagen);
    fd.append('idCategoria', this.idCategoria);
    fd.append('idMarca', this.idMarca);

    this.http.post("http://localhost/Api/operaciones.php?opcion=1", fd).subscribe(res => {
      
      this.router.navigate(['lista-vehiculos']);
    });
  }


}



