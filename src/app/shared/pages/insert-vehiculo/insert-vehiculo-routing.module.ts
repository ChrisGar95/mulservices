import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InsertVehiculoPage } from './insert-vehiculo.page';

const routes: Routes = [
  {
    path: '',
    component: InsertVehiculoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InsertVehiculoPageRoutingModule {}
