import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InsertVehiculoPageRoutingModule } from './insert-vehiculo-routing.module';

import { InsertVehiculoPage } from './insert-vehiculo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InsertVehiculoPageRoutingModule
  ],
  declarations: [InsertVehiculoPage]
})
export class InsertVehiculoPageModule {}
