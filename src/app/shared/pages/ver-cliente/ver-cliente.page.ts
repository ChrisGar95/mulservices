import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-ver-cliente',
  templateUrl: './ver-cliente.page.html',
  styleUrls: ['./ver-cliente.page.scss'],
})
export class VerClientePage implements OnInit {

  cliente: any="";
  codigo:any="";
  ruc:any="";
  nombre:any="";
  nombreComercial:any="";
  ciudad:any="";
  direccion:any="";
  telefono:any="";
  correo:any="";

  constructor
  (
    public router: Router,
    private route: ActivatedRoute,
    private http:HttpClient,
    public alertController: AlertController
  ) 
  {

  }

  async ngOnInit() {
     const id = this.route.snapshot.paramMap.get("id");
    this.http.get("http://sagacorp.live/Api/clientes.php?opcion=2&clicod="+id).subscribe(snap => {
      this.cliente = snap;
      this.codigo=this.cliente[0].clicod;
      this.ruc=this.cliente[0].cliruc;
      this.nombre=this.cliente[0].clinom;
      this.nombreComercial=this.cliente[0].clinomcom;
      this.ciudad=this.cliente[0].cliciu;
      this.direccion=this.cliente[0].clidir;
      this.telefono=this.cliente[0].clitel;
      this.correo=this.cliente[0].cliemail;
    });
  }

  actualizaDatosCliente()
  {
    var validaCampos=0;
    if(this.nombre=="")
    {
      validaCampos=1;
      this.presentAlert("Ingresar razon social");
    }
    if(this.ruc=="")
    {
      validaCampos=1;
      this.presentAlert("Ingresar ruc");
    }
    if(this.nombreComercial=="")
    {
      validaCampos=1;
      this.presentAlert("Ingresar nombre comercial");
    }
  
    if(this.direccion=="")
    {
      validaCampos=1;
      this.presentAlert("Ingresar direccion");
    }
    if(this.telefono=="")
    {
      validaCampos=1;
      this.presentAlert("Ingresar telefono");
    }

    if(validaCampos==0)
    {
      var fd = new FormData();

      fd.append('codigo',this.codigo);
      fd.append('ruc', this.ruc);
      fd.append('nombre', this.nombre);
      fd.append('nombreComercial', this.nombreComercial);
      fd.append('ciudad', this.ciudad);
      fd.append('direccion', this.direccion);
      fd.append('telefono', this.telefono);
      fd.append('correo', this.correo);
  
      this.http.post("http://sagacorp.live/Api/clientes.php?opcion=6", fd).subscribe(res => {
         if(res){
          this.router.navigate(['lista-clientes']);
         }
       });
    }
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: message,
      //subHeader: 'Subtitle',
      //message: message,
      buttons: ['OK']
    });

    await alert.present();
  }
}
