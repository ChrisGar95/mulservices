import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Router, NavigationEnd  } from '@angular/router';


@Component({
  selector: 'app-lista-clientes',
  templateUrl: './lista-clientes.page.html',
  styleUrls: ['./lista-clientes.page.scss'],
})
export class ListaClientesPage {
 /*usuarios: Observable<Usuario[]>;

  constructor(private usuarioService: UsuariosService) { }*/
  listado: any;
  listadoResp: any;
  
  constructor(public navCtrl: NavController, private http:HttpClient,public router: Router) 
  {
    this.listado="";
    this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationEnd) {
        console.log("actualizado");
        this.cargarInformacion();
      }
    });
  }

  async ngOnInit() {

    //this.cargarInformacion();
    
    /*this.http.get("http://sagacorp.live/Api/clientes.php?opcion=1").subscribe(snap => {
      this.listado = snap;
      
    });*/
  }

  async cargarInformacion() 
  {
    
    this.http.get("http://sagacorp.live/Api/clientes.php?opcion=1").subscribe(snap => {
      this.listadoResp = snap;
      this.listado=this.listadoResp;
    });
  }

  showCliente(id: any)
  {
    this.router.navigate([`ver-cliente/${id}`])
    
  }

  async filterList(evt) 
  {
    const searchTerm = evt.srcElement.value.trim();

    if (!searchTerm) 
    {
      this.listado = this.listadoResp;
      return;
    }
    else
    {

      this.listado = this.listadoResp;
      this.listado = this.listado.filter(item => {
      if (item.clinom && searchTerm) {
        return (item.clinom.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 );
      }
    });

    }

  }


}

