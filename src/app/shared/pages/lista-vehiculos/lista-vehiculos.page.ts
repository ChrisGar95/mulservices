import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Router, NavigationEnd  } from '@angular/router';
import { analytics } from 'firebase';


@Component({
  selector: 'app-lista-vehiculos',
  templateUrl: './lista-vehiculos.page.html',
  styleUrls: ['./lista-vehiculos.page.scss'],
})
export class ListaVehiculosPage implements OnInit {
 
  listado: any;
  listadoResp: any;
  
  constructor(public navCtrl: NavController, private http:HttpClient,public router: Router) 
  {
    this.listado="";
    this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationEnd) {
        console.log("actualizado");
        this.cargarInformacion();
      }
    });
  }

  async ngOnInit() {

  }

  async cargarInformacion() 
  {
    
    this.http.get("http://localhost:8081/api/getVehicles").subscribe(snap => {
      this.listadoResp = snap;
      this.listado=this.listadoResp;
    });
  }

  showVehiculo(id: any)
  {
    this.router.navigate([`ver-vehiculo/${id}`])
    
  }

  async filterList(evt) 
  {
    const searchTerm = evt.srcElement.value.trim();

    if (!searchTerm) 
    {
      this.listado = this.listadoResp;
      return;
    }
    else
    {

      this.listado = this.listadoResp;
      this.listado = this.listado.filter(item => {
      if ((item.modelo && searchTerm) || (item.idMarca && searchTerm)){

        var devuelve;

        devuelve=(item.modelo.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 );
        
        if(devuelve=="")
        {
          devuelve=(item.idMarca.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 ); 
        }
        if(devuelve=="")
        {
          devuelve=(item.idCategoria.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 ); 
        }
        

        return devuelve;
        
      }

      
      
    });

    }

  }


}

