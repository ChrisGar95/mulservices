import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AlertController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-ver-vehiculo',
  templateUrl: './ver-vehiculo.page.html',
  styleUrls: ['./ver-vehiculo.page.scss'],
})
export class VerVehiculoPage implements OnInit {

  idCategoria: any="";
  vehiculos: any="";
  vehiculo: any="";
  idMarca:any="";
  anio:any="";
  modelo:any="";
  version:any="";
  color:any="";
  imagen:any="";
  precio:any="";
  

  constructor
  (
    public router: Router,
    private route: ActivatedRoute,
    private http:HttpClient,
    public alertController: AlertController,
    private domSanitizer: DomSanitizer
  ) 
  {

  }

  async ngOnInit() {
    const id = this.route.snapshot.paramMap.get("id");
    this.http.get("http://localhost:8081/api/getVehicles").subscribe(snap => {
      this.vehiculos = snap;

    for(let i = 0; i < this.vehiculos.length; i++) 
    {
      
      if(this.vehiculos[i].id==id)
      {
        this.idCategoria=this.vehiculos[i].idCategoria;
        this.idMarca=this.vehiculos[i].idMarca;
        this.modelo=this.vehiculos[i].modelo;
        this.anio=this.vehiculos[i].anio;
        this.version=this.vehiculos[i].version;
        this.color=this.vehiculos[i].color;
        this.imagen = this.vehiculos[i].imagen;
        this.precio = this.vehiculos[i].precio;
      }
    }
    });
  }

  /*
  actualizaDatosCliente()
  {
    var validaCampos=0;
    if(this.nombre=="")
    {
      validaCampos=1;
      this.presentAlert("Ingresar razon social");
    }
    if(this.ruc=="")
    {
      validaCampos=1;
      this.presentAlert("Ingresar ruc");
    }
    if(this.nombreComercial=="")
    {
      validaCampos=1;
      this.presentAlert("Ingresar nombre comercial");
    }
  
    if(this.direccion=="")
    {
      validaCampos=1;
      this.presentAlert("Ingresar direccion");
    }
    if(this.telefono=="")
    {
      validaCampos=1;
      this.presentAlert("Ingresar telefono");
    }

    if(validaCampos==0)
    {
      var fd = new FormData();

      fd.append('codigo',this.codigo);
      fd.append('ruc', this.ruc);
      fd.append('nombre', this.nombre);
      fd.append('nombreComercial', this.nombreComercial);
      fd.append('ciudad', this.ciudad);
      fd.append('direccion', this.direccion);
      fd.append('telefono', this.telefono);
      fd.append('correo', this.correo);
  
      this.http.post("http://sagacorp.live/Api/clientes.php?opcion=6", fd).subscribe(res => {
         if(res){
          this.router.navigate(['lista-clientes']);
         }
       });
    }
  }*/

  async presentAlert(message) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: message,
      //subHeader: 'Subtitle',
      //message: message,
      buttons: ['OK']
    });

    await alert.present();
  }
}
