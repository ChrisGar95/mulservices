GEOLOCALIZACION 

1. Instalamos, para mayor documentacion visitar: https://ionicframework.com/docs/native/geolocation

![1](Images/1.png)

2. Importamos en nuestro archivo app.module.ts

![2](Images/2.png)

   y en nuestro archivo.ts donde tengamos nuestro componente

3.  Vamos a crear tres variables, vamos a inicializar un objeto de Geolocalitation en nuestro constructor y una función en nuestro testcomponent.component.ts (en mi caso) que llamaremos getGeolocation(), que utilizaremos para obtener la latitud y longitud de nuestra posición. Ahora que ya tenemos nuestras coordenas y calcularemos la distancia entre nosotros y un punto determinado.

En este caso, supongamos que quiero saber la distancia que hay entre mi posición y algún local que se encuentre en Madrid.

De esta forma nuestro componente testcomponent.component.ts (en mi caso) quedaria de la siguiente manera:

![3](Images/3.png)

![4](Images/4.png)

![5](Images/5.png)

Y nuestro html de nuestro componente testcomponent.component.html (en mi caso) quedaria de la siguiente manera:

![6](Images/6.png)

Y por ultimo lo llamamos en nuestra pagina.html en la que queramos usar nuestro componente

![7](Images/7.png)

Y tendremos algo como lo siguiente:

![8](Images/8.png)
![9](Images/9.png)


